﻿#include <iostream>

void PrintNumber(int m, bool e) 
{
    if (e == false)
    {
        std::cout << "Even numbers: ";
    }
    else
        std::cout << "Odd numbers: ";

    for (int x = e; x <= m; x+=2)
    {
        std::cout << x << " ";             
    }
}

int main()
{
    int MaxNumber;
    int bl;
    std::cout << "Enter '0' to print even numbers or any other number to print odd numbers: ";
    std::cin >> bl;
    std::cout << "Enter the maximum number: ";
    std::cin >> MaxNumber;
    
    PrintNumber(MaxNumber, bl);
    return 0;
}